<?php

//TODO: Interface to configure what is classed a duplicate on each entity type. Should this be a rules integration? It just taps into the hook of entity_duplicate_scan, replacing the default of identical title. Select an entity type, choose the fields that should be unique (and/or?), and select a default as the entity to merge to.

//ToDo: Rules integration, on: [create entity], scan for duplicates, and if duplicate found, present a notice with a cancel or proceed anyway option.

//Refer to tut: http://dominiquedecooman.com/blog/drupal-custom-rules-how-write-your-own-events-conditions-actions-and-custom-object-custom-token